/*
    Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/


#include "GaudiKernel/LoadFactoryEntries.h"

LOAD_FACTORY_ENTRIES(eFEXDriver)
LOAD_FACTORY_ENTRIES(eFEXNtupleWriter)
  //Notes:
  //
  //1. The argument to the LOAD_FACTORY_ENTRIES() is the name of the
  //   component library (libXXX.so).
  //
  // See Athena User Guide for more information
